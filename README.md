# tinc-tools

Misc tools for Tinc VPN.

## Tinc VPN mapping

### ip_extract.py

Extracts VPN clients IP from the DHCP leases file.

### tinc2png

Turns the `GraphDumpFile` output to a PNG file.

### How to deploy

```ini
GraphDumpFile = | egrep -i -v 'salade|steven|Florian' | /usr/local/sbin/ip_extract.py | /usr/local/sbin/tinc2png testvpn
```

## Tinc leases listing

### tinc-leases.py

List leases from ISC DHCPd leases file.

#### Usage

```shell
./tinc-leases.py
```

In this output example, the third device didn't send its hostname:

```text
02:bf:a2:d4:05:b0 10.90.100.7 idc_bsf_demo_1234
02:19:78:04:54:f6 10.90.100.135 idc_bsf_demo_5678
02:99:54:bd:f6:00 10.90.100.25
```

#### Installation

This script requires
[isc_dhcp_leases](https://packages.debian.org/python3-isc-dhcp-leases),
packaged in Debian:

```shell
apt install python3-isc-dhcp-leases
```

#### Performances

The script is slower than `dhcp-lease-list(1)`:

```text
# hyperfine "dhcp-lease-list" "./tinc-leases.py" --export-markdown -
Benchmark 1: dhcp-lease-list
  Time (mean ± σ):     121.1 ms ±   9.1 ms    [User: 98.3 ms, System: 20.2 ms]
  Range (min … max):   113.2 ms … 158.2 ms    24 runs

Benchmark 2: ./tinc-leases.py
  Time (mean ± σ):     200.5 ms ±  15.1 ms    [User: 185.0 ms, System: 15.1 ms]
  Range (min … max):   191.7 ms … 253.0 ms    15 runs

Summary
  dhcp-lease-list ran
    1.66 ± 0.18 times faster than ./tinc-leases.py
```

| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `dhcp-lease-list` | 121.1 ± 9.1 | 113.2 | 158.2 | 1.00 |
| `./tinc-leases.py` | 200.5 ± 15.1 | 191.7 | 253.0 | 1.66 ± 0.18 |

However:

- 121ms vs 200ms is not noticable for humans
- its output is simpler than `dhcp-lease-list --parsable`
- it may integrate better with python tooling

Overall, it is considered Good Enough™.

## Tinc connections log

### tinc-connections-summary.sh

Shows the latest connections to the VPN.

It takes a number of lines to show as an argument (default is 30).

#### Usage

```shell
tinc-connections-summary.sh 13
```

```text
2024-03-14 23:44:25 : Connection with ideascube_1138 activated
2024-03-15 03:00:33 : Closing connection with ideascube_1138
2024-03-15 03:01:00 : Connection with ideascube_1138 activated
2024-03-16 03:01:09 : Closing connection with ideascube_1138
2024-03-16 03:01:17 : Connection with ideascube_1138 activated
2024-03-17 03:00:26 : Closing connection with ideascube_1138
2024-03-17 03:01:11 : Connection with ideascube_1138 activated
2024-03-18 03:01:03 : Closing connection with ideascube_1138
2024-03-18 03:01:03 : Connection with ideascube_1138 activated
2024-03-18 10:36:08 : Closing connection with ideascube_0bc1
2024-03-18 10:36:22 : Closing connection with ideascube_1138
2024-03-18 10:36:28 : Connection with ideascube_1138 activated
2024-03-18 10:37:21 : Connection with ideascube_0bc1 activated
```
