#!/usr/bin/env python3

import os, sys

from isc_dhcp_leases import Lease, IscDhcpLeases


LEASE_FILE = os.environ.get("LEASE_FILE", "/var/lib/dhcp/dhcpd.leases")


def mac_from_label(label):
    try:
        return label.rsplit("_", 1)[1]
    except:
        return None


def get_ip_from_mac(mac):
    for lease in current_leases:
        if lease.hostname.endswith("-" + mac):
            return lease.ip
    return ""


current_leases = IscDhcpLeases(LEASE_FILE).get_current().values()

for line in sys.stdin:
    if "label" in line:
        node_name = line.split('"')[1]
        label = line.split('"')[3]
        mac = mac_from_label(label)
        if mac:
            ip = get_ip_from_mac(mac)
            label = f"{label}\\n{ip}" if ip else label
        line = f'    "{node_name}" [ label = "{label}" ];'
    print(line)
